	Funcionamento
	
	
	
	O programa foi criado utilizando python3 e para roda-lo corretamente é necessário instalar a biblioteca "psycopg2".
	
	Além disso, é necessário mudar o código da conexão com o Banco de Dados, colocando o nome do banco, o usuário,
	a senha e o host na linha 23.
	
	Para gerar o Banco de Dados foi utilizado o Modelo Lógico que está no arquivo "theLifeSnakeWork.sql" na
	pasta "03b - Modelo Lógico - DDL" do projeto.