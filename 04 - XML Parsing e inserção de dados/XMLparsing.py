from urllib.request import urlopen
from xml.dom.minidom import parse
import xml.dom.minidom

import psycopg2

url_person = urlopen(
    'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml')
parse_person = xml.dom.minidom.parse(url_person)


url_music = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml')
parse_music = xml.dom.minidom.parse(url_music)

url_movie = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml')
parse_movie = xml.dom.minidom.parse(url_movie)

url_knows = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml')
parse_knows = xml.dom.minidom.parse(url_knows)

#   Mudar a linha abaixo com as configurações do Banco de Dados do usuário.

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="12345", host="127.0.0.1")
cur = conn.cursor()

persons = parse_person.getElementsByTagName('Person')
musics = parse_music.getElementsByTagName('LikesMusic')
movies = parse_movie.getElementsByTagName('LikesMovie')
knows = parse_knows.getElementsByTagName('Knows')


def exist(table, col1, one, col2=None, two=None):
    if two is None:
        cur.execute(
            "SELECT * FROM {} WHERE {} = '{}'".format(table, col1, one))
    else:
        cur.execute(
            "SELECT * FROM {} WHERE {} = '{}' AND {} = '{}'".format(table, col1, one, col2, two))

    if len(cur.fetchall()) > 0:
        return True
    else:
        return False


def exec_usuario():

    for person in persons:

        if person.getAttribute('uri'):
            uri = person.getAttribute('uri')
        else:
            uri = None

        if person.getAttribute('name'):
            name = person.getAttribute('name')
        else:
            name = None

        if person.getAttribute('hometown'):
            hometown = person.getAttribute('hometown')
        else:
            hometown = None

        if person.getAttribute('birthdate'):
            birthdate = person.getAttribute('birthdate')
        else:
            birthdate = None

        if not exist("usuario", "login", person):
            cur.execute("INSERT INTO usuario VALUES (%s, %s, %s, %s)", (
                uri, name, hometown, birthdate))
    conn.commit()


def exec_registra():

    for know in knows:
        if know.getAttribute('person'):
            person = know.getAttribute('person')
        else:
            person = None

        if know.getAttribute('colleague'):
            colleague = know.getAttribute('colleague')
        else:
            colleague = None

        if not exist("registra", "usr1", person, "usr2", colleague):
            cur.execute("INSERT INTO registra VALUES ('{}', '{}')".format(
                person, colleague))
    conn.commit()


def exec_artista_musical():

    for music in musics:
        if music.getAttribute('bandUri'):
            uri = music.getAttribute('bandUri')
        else:
            uri = None
        if not exist("artista_musical", "banda_uri", uri):
            cur.execute(
                "INSERT INTO artista_musical VALUES ('{}')".format(uri))
    conn.commit()


def exec_curte_artista_musical():

    for music in musics:
        if music.getAttribute('person'):
            person = music.getAttribute('person')
        else:
            person = None

        if music.getAttribute('rating'):
            rating = music.getAttribute('rating')
        else:
            rating = None

        if music.getAttribute('bandUri'):
            uri = music.getAttribute('bandUri')
        else:
            uri = None

        if not exist("curte_artista_musical", "usr", person, "banda_uri", uri):
            cur.execute("INSERT INTO curte_artista_musical VALUES ('{}', '{}', '{}')".format(
                person, uri, rating))
    conn.commit()


def exec_filme():

    for movie in movies:
        if movie.getAttribute('movieUri'):
            uri = movie.getAttribute('movieUri')
        else:
            uri = None

        if not exist("filme", "filme_uri", uri):
            cur.execute(
                "INSERT INTO filme VALUES ('{}')".format(uri))
    conn.commit()



def exec_curte_filme():

    for movie in movies:
        if movie.getAttribute('person'):
            person = movie.getAttribute('person')
        else:
            person = None

        if movie.getAttribute('rating'):
            rating = movie.getAttribute('rating')
        else:
            rating = None

        if movie.getAttribute('movieUri'):
            uri = movie.getAttribute('movieUri')
        else:
            uri = None

        if not exist("curte_filme", "usr", person, "filme_uri", uri):
            cur.execute("INSERT INTO curte_filme VALUES ('{}', '{}', '{}')".format(
                person, uri, rating))
    conn.commit()


exec_usuario()
exec_registra()
exec_artista_musical()
exec_curte_artista_musical()
exec_filme()
exec_curte_filme()
