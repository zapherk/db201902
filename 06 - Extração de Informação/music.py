from bs4 import BeautifulSoup
import requests
import psycopg2
import xml.etree.cElementTree as ET

conn = psycopg2.connect(dbname="201902The_Life_Snake", user="201902The_Life_Snake",
                        password="425802", host="200.134.10.32")
cur = conn.cursor()

content = ET.Element("content")
tree = ET.ElementTree(content)

cur.execute("SELECT banda_uri FROM artista_musical")
urls = [row[0] for row in cur.fetchall()]

count = 0

for url in urls:
   
    count += 1
    identificador = str(count)

    artista_musical = ET.SubElement(content, "artista_musical", id=identificador)
    ET.SubElement(artista_musical, "URI").text = url

    page = requests.get(url)

    soup = BeautifulSoup(page.text, 'html.parser')

    if soup.find_all('table', class_='infobox vcard plainlist'):
        data = soup.find_all('table', class_='infobox vcard plainlist')

        if not data:
            break

        data = data[0]

        nome = soup.select('div[class*="fn org"]')
        if(nome):
            print("Nome: ", nome[0].get_text())
            ET.SubElement(artista_musical, "Nome").text = nome[0].get_text()
        else:
            nome = soup.select('div[class*="fn"]')
            if(nome):
                print("Nome: ", nome[0].get_text())
                ET.SubElement(artista_musical, "Nome").text = nome[0].get_text()

        for row in data.find_all('tr'):
            if(row.get_text().find("Origin") >= 0):
                origem = row.get_text()
                origem = origem[6:]
                print("Origem:", origem)
                ET.SubElement(artista_musical, "Origem").text = origem
            if(row.get_text().find("Genres") >= 0):
                generos = row.get_text()
                generos = generos[6:]
                print("Gêneros:", generos)
                ET.SubElement(artista_musical, "Generos").text = generos  
            if(row.get_text().find("Years active") >= 0):
                yearsActive = row.get_text()
                yearsActive = yearsActive[12:]
                print("Atividade:", yearsActive)
                ET.SubElement(artista_musical, "Atividade").text = yearsActive
            if(row.get_text().find("Members") >= 0 and row.get_text().find("Past members") == -1):
                members = row.get_text()
                members = members[7:]
                print("Membros:", members)
                ET.SubElement(artista_musical, "Membros").text = members
    else:   
        data = soup.find_all('table', class_='infobox biography vcard')
        
        if not data:
            break

        data = data[0]
        
        nome = soup.select('div[class*="fn"]')
        if(nome):
            print("Nome: ", nome[0].get_text())
            ET.SubElement(artista_musical, "Nome").text = nome[0].get_text()
        else:
            nome = soup.select('div[class*="fn org"]')
            if(nome):
                print("Nome: ", nome[0].get_text())
                ET.SubElement(artista_musical, "Nome").text = nome[0].get_text()

        origem = soup.select('div[class*="birthplace"]')
        if(origem):
            print("Origem:", origem[0].get_text())
            ET.SubElement(artista_musical, "Origem").text = origem[0].get_text()

        for row in data.find_all('tr'):
            if(row.get_text().find("Genres") >= 0):
                generos = row.get_text()
                generos = generos[6:]
                print("Gêneros:", generos)
                ET.SubElement(artista_musical, "Generos").text = generos  
            if(row.get_text().find("Years active") >= 0):
                yearsActive = row.get_text()
                yearsActive = yearsActive[12:]
                print("Atividade:", yearsActive)
                ET.SubElement(artista_musical, "Atividade").text = yearsActive
    print("-----------------------------------------------------------------")

tree.write("music.xml")

    