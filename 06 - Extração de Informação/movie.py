#!/usr/bin/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import psycopg2
import xml.etree.cElementTree as ET

conn = psycopg2.connect(dbname="201902The_Life_Snake", user="201902The_Life_Snake",
                        password="425802", host="200.134.10.32")
cur = conn.cursor()

data = ET.Element("data")
tree = ET.ElementTree(data)

cur.execute("select filme_uri from filme")
urls = [row[0] for row in cur.fetchall()]

count = 0 # Somente para gera um id para o filme no xml.

for url in urls:

    count += 1
    identificador = str(count)

    filme = ET.SubElement(data, "Filme", id=identificador)
    ET.SubElement(filme, "URI").text = url

    page = requests.get(url)

    soup = BeautifulSoup(page.text, 'html.parser')
    
    titulo = soup.select('div[class*="originalT"]')
    for t in titulo:
        if(t != None):
            print(t.get_text())
            ET.SubElement(filme, "Nome").text = t.get_text()
        else:
            print("---")
            ET.SubElement(filme, "Nome").text = "---"
    
    lancamento = soup.select('a[href*="/releaseinfo"]')
    for l in lancamento:
        if(l != None):
            if(l.get_text() != "Release Dates"):
                print(l.get_text())
                ET.SubElement(filme, "Lancamento").text = l.get_text()
        else:
            print("---")
            ET.SubElement(filme, "Lancamento").text = "---"

    equipe = ""
    diretorEscritorAtores = soup.select('div[class*="credit_summary_item"]')
    for d in diretorEscritorAtores:
        if(d != None):
            equipe = equipe + d.get_text()

    if (equipe.find("|") >= 0): # Remove parte do texto que vem além do necessário.
        pos = equipe.index("|")
        equipe = equipe[:pos]

    print(equipe)
    ET.SubElement(filme, "Equipe").text = equipe

    print("")
    print("Gêneros:")
    generos = ""
    genero = soup.select('a[href*="/search/title?genres"]')
    for g in genero:
        if(g != None):
            if (generos.find(g.get_text().lstrip()) < 0): # Garante que não vai vir gênero duplicado.
                print(g.get_text().lstrip())
                generos = generos + g.get_text().lstrip() + " "
        else:
            print("---")
    
    ET.SubElement(filme, "Generos").text = generos
    
    print("-----------------------------------------------------------------")

tree.write("movie.xml")