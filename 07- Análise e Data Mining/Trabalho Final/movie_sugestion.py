import pandas as pd
import surprise

from flask import Flask, jsonify, request
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_DBNAME"] = 'final_db'
#app.config["MONGO_URI"] = 'mongodb+srv://rafh:admin@cluster0-gaog1.mongodb.net/final_db?retryWrites=true&w=majority'
app.config["MONGO_URI"] = 'mongodb+srv://rafh:admin@final-xkf32.gcp.mongodb.net/final_db?retryWrites=true&w=majority'

mongo = PyMongo(app)

curte_artista_musical = mongo.db.curte_artista_musical
sugestao = mongo.db.sugestao_artista

curteFilme = list(curte_artista_musical.find())
#print(curteFilme[0]['banda_nome'])

ratings_movies = {}
ratings_movies['usr'] = []
ratings_movies['banda_nome'] = []
ratings_movies['nota'] = []

top3 = {}
curtidos = {}

for i in curteFilme:
    ratings_movies['usr'].append(i['usr'])
    ratings_movies['banda_nome'].append(i['banda_nome'])
    ratings_movies['nota'].append(i['nota'])

    #a logica deve ta meio bosta mas foi o q tinha pra hj
    if not i['usr'] in top3.keys():
        top3[i['usr']] = []
        curtidos[i['usr']] = []

    curtidos[i['usr']].append(i['banda_nome'])

df = pd.DataFrame(ratings_movies)

print(df)

reader = surprise.Reader(rating_scale=(1, 5))

# The columns must correspond to user id, item id and ratings (in that order).
data = surprise.Dataset.load_from_df(df[['usr', 'banda_nome', 'nota']], reader)

trainset = data.build_full_trainset()

algo = surprise.KNNBasic()
algo.fit(trainset)

for i in range (1,len(ratings_movies['usr'])):
    for j in range (1,len(ratings_movies['banda_nome'])):
        filme_var = str(ratings_movies['banda_nome'][j])
        usr_var = str(ratings_movies['usr'][i])
        if not filme_var in curtidos[usr_var]: #se o usuario não curtiu esse filme, evita recomendar filmes ja vistos
            pred = algo.predict(uid = usr_var, iid = filme_var)
            score = float(pred.est)
            #top3 no formato filme1[0], nota1[1], filme2[2], nota2[3], filme3[4], nota3[5]
            if(len(top3[usr_var]) <= 4): #se o top3 tem menos de 3 filmes cadastrados        
                top3[usr_var].append(filme_var)
                top3[usr_var].append(score)
                if i == 1: #apenas para teste
                    print("menos de 3 sugeridos")
                    print(top3[usr_var])
            else: #ja tem 3 sugestoes, remove a com o menor score
                ind = top3[usr_var].index(min(top3[usr_var][1], top3[usr_var][3], top3[usr_var][5]))
                if score > top3[usr_var][ind]:
                    del top3[usr_var][ind - 1]
                    del top3[usr_var][ind - 1]
                    top3[usr_var].append(filme_var)
                    top3[usr_var].append(score)
                    if i == 1: #apenas para teste
                        print("atualizando o com o menor rating dos 3 sugeridos")
                        print(top3[usr_var])

for key, value in top3.items():
    key = key.split('/')[-1]
    sug = {
        'login': key,
        'sugestao1': value[0],
        'sugestao2': value[2],
        'sugestao3': value[4]
    }

    sugestao.insert_one(sug)

