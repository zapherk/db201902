from urllib.request import urlopen
from xml.dom.minidom import parse
import xml.dom.minidom
from datetime import datetime

import requests
from bs4 import BeautifulSoup
import xml.etree.cElementTree as ET

from pymongo import MongoClient

# --- MONGO DB --- #
#MONGO_URI = 'mongodb+srv://rafh:admin@cluster0-gaog1.mongodb.net/test?retryWrites=true&w=majority'
MONGO_URI = 'mongodb+srv://rafh:admin@final-xkf32.gcp.mongodb.net/final_db?retryWrites=true&w=majority'

client = MongoClient(MONGO_URI)
db = client.get_database('final_db')

usuario = db.usuario
registra = db.registra
artista_musical = db.artista_musical
filme = db.filme
curte_artista_musical = db.curte_artista_musical
curte_filme = db.curte_filme

# --- XML PARSING --- #

url_person = urlopen(
    'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml')
parse_person = xml.dom.minidom.parse(url_person)


url_music = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml')
parse_music = xml.dom.minidom.parse(url_music)

url_movie = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml')
parse_movie = xml.dom.minidom.parse(url_movie)

url_knows = urlopen('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml')
parse_knows = xml.dom.minidom.parse(url_knows)

persons = parse_person.getElementsByTagName('Person')
musics = parse_music.getElementsByTagName('LikesMusic')
movies = parse_movie.getElementsByTagName('LikesMovie')
knows = parse_knows.getElementsByTagName('Knows')

data = ET.Element("data")
tree = ET.ElementTree(data)

content = ET.Element("content")
ctree = ET.ElementTree(content)

# --- INSERT FUNCTIONS --- #

def existUser(login):
    return bool(db.usuario.find_one({'login': login}))

def existRegistra(person, colleague):
    return bool(db.registra.find_one({'usr1': person, 'usr2': colleague}))

def existArtista(uri):
    return bool(db.artista_musical.find_one({'banda_uri': uri}))

def existCurteArtista(person, banda_uri):
    return bool(db.curte_artista_musical.find_one({'usr': person, 'banda_uri': banda_uri}))

def existFilme(uri):
    return bool(db.filme.find_one({'filme_uri': uri}))

def existCurteFilme(person, filme_uri):
    return bool(db.curte_filme.find_one({'usr': person, 'filme_uri': filme_uri}))



def exec_usuario():

    for person in persons:

        if person.getAttribute('uri'):
            uri = person.getAttribute('uri')
        else:
            uri = None

        if person.getAttribute('name'):
            name = person.getAttribute('name')
        else:
            name = None

        if person.getAttribute('hometown'):
            hometown = person.getAttribute('hometown')
        else:
            hometown = None

        if person.getAttribute('birthdate'):
            birthdate = person.getAttribute('birthdate')
            birthdate = birthdate.split('-')
        else:
            birthdate = None

        uri = uri.split('/')[-1]

        if birthdate is not None:
            user = {
                'login': uri,
                'nome_completo': name,
                'cidade_natal': hometown,
                'data_nascimento': datetime(int(birthdate[0]), int(birthdate[1]), int(birthdate[2]))
            }
        else:
            user = {
                'login': uri,
                'nome_completo': name,
                'cidade_natal': hometown,
                'data_nascimento': ''
            }

        if not existUser(uri):
            usuario.insert_one(user)
        else:
            print("User already exist!")

def exec_registra():

    for know in knows:
        if know.getAttribute('person'):
            person = know.getAttribute('person')
        else:
            person = None

        if know.getAttribute('colleague'):
            colleague = know.getAttribute('colleague')
        else:
            colleague = None

        person = person.split('/')[-1]
        colleague = colleague.split('/')[-1]

        reg = {
            'usr1': person,
            'usr2': colleague
        }

        if not existRegistra(person, colleague):
            registra.insert_one(reg)
        else:
            print("Registro já existe!")

def exec_artista_musical():

    for music in musics:
        if music.getAttribute('bandUri'):
            uri = music.getAttribute('bandUri')
        else:
            uri = None

        if not existArtista(uri):

            artista_musicals = ET.SubElement(content, "artista_musical")
            ET.SubElement(artista_musicals, "URI").text = uri

            page = requests.get(uri)

            soup = BeautifulSoup(page.text, 'html.parser')

            if soup.find_all('table', class_='infobox vcard plainlist'):
                data = soup.find_all('table', class_='infobox vcard plainlist')

                if not data:
                    break

                data = data[0]

                nome = soup.select('div[class*="fn org"]')
                if(nome):
                    #print("Nome: ", nome[0].get_text())
                    ET.SubElement(artista_musicals, "Nome").text = nome[0].get_text()
                else:
                    nome = soup.select('div[class*="fn"]')
                    if(nome):
                        #print("Nome: ", nome[0].get_text())
                        ET.SubElement(artista_musicals, "Nome").text = nome[0].get_text()

            print(nome[0].get_text())

            artista = {
            'banda_uri': uri,
            'banda_nome': nome[0].get_text()
            }

            artista_musical.insert_one(artista)
        else:
            print("Artista already exist!")

def exec_curte_artista_musical():

    for music in musics:
        if music.getAttribute('person'):
            person = music.getAttribute('person')
        else:
            person = None

        if music.getAttribute('rating'):
            rating = music.getAttribute('rating')
        else:
            rating = None

        if music.getAttribute('bandUri'):
            uri = music.getAttribute('bandUri')
        else:
            uri = None

        person = person.split('/')[-1]

        if not existCurteArtista(person, uri):


            artista_musicals = ET.SubElement(content, "artista_musical")
            ET.SubElement(artista_musicals, "URI").text = uri

            page = requests.get(uri)

            soup = BeautifulSoup(page.text, 'html.parser')

            if soup.find_all('table', class_='infobox vcard plainlist'):
                data = soup.find_all('table', class_='infobox vcard plainlist')

                if not data:
                    break

                data = data[0]

                nome = soup.select('div[class*="fn org"]')
                if(nome):
                    #print("Nome: ", nome[0].get_text())
                    ET.SubElement(artista_musicals, "Nome").text = nome[0].get_text()
                else:
                    nome = soup.select('div[class*="fn"]')
                    if(nome):
                        #print("Nome: ", nome[0].get_text())
                        ET.SubElement(artista_musicals, "Nome").text = nome[0].get_text()

            print(nome[0].get_text())

            curte = {
            'usr': person,
            'banda_uri': uri,
            'banda_nome': nome[0].get_text(),
            'nota': rating
            }


            curte_artista_musical.insert_one(curte)
        else:
            print("User already likes it!")

def exec_filme():

    for movie in movies:
        if movie.getAttribute('movieUri'):
            uri = movie.getAttribute('movieUri')
        else:
            uri = None

        if not existFilme(uri):
            fil = ET.SubElement(data, "Filme")
            ET.SubElement(fil, "URI").text = uri

            page = requests.get(uri)

            soup = BeautifulSoup(page.text, 'html.parser')
            
            titulo = soup.select('div[class*="originalT"]')
            for t in titulo:
                if(t != None):
                    print(t.get_text())
                    ET.SubElement(fil, "Nome").text = t.get_text()
                else:
                    print("---")
                    ET.SubElement(fil, "Nome").text = "---"

            if t.get_text():
                movie = {
                    'filme_uri': uri,
                    'filme_nome': t.get_text()
                }
            else:
                movie = {
                    'filme_uri': uri,
                    'filme_nome': None
                }
            filme.insert_one(movie)
        else:
            print("Movie already exist!")

def exec_curte_filme():

    for movie in movies:
        if movie.getAttribute('person'):
            person = movie.getAttribute('person')
        else:
            person = None

        if movie.getAttribute('rating'):
            rating = movie.getAttribute('rating')
        else:
            rating = None

        if movie.getAttribute('movieUri'):
            uri = movie.getAttribute('movieUri')
        else:
            uri = None

        person = person.split('/')[-1]

        if not existCurteFilme(person, uri):

            fil = ET.SubElement(data, "Filme")
            ET.SubElement(fil, "URI").text = uri

            page = requests.get(uri)

            soup = BeautifulSoup(page.text, 'html.parser')
            
            titulo = soup.select('div[class*="originalT"]')
            for t in titulo:
                if(t != None):
                    print(t.get_text())
                    ET.SubElement(fil, "Nome").text = t.get_text()
                else:
                    print("---")
                    ET.SubElement(fil, "Nome").text = "---"

            if t.get_text():
                curte = {
                    'usr': person,
                    'filme_uri': uri,
                    'filme_nome': t.get_text(),
                    'nota': rating
                    }
            else:
               curte = {
                    'usr': person,
                    'filme_uri': uri,
                    'filme_nome': None,
                    'nota': rating
                    }

            


            curte_filme.insert_one(curte)
        else:
            print("User already likes it!")

exec_usuario()
exec_registra()
exec_artista_musical()
exec_curte_artista_musical()
exec_filme()
exec_curte_filme()