from flask import Flask, jsonify, request, session, redirect, url_for, Response
from flask_pymongo import PyMongo

from bson.json_util import dumps
from bson.objectid import ObjectId

from datetime import datetime
import bcrypt

app = Flask(__name__)
app.secret_key = 'secret'
app.config["MONGO_DBNAME"] = 'final_db'
#app.config["MONGO_URI"] = 'mongodb+srv://rafh:admin@cluster0-gaog1.mongodb.net/final_db?retryWrites=true&w=majority'
app.config["MONGO_URI"] = 'mongodb+srv://rafh:admin@final-xkf32.gcp.mongodb.net/final_db?retryWrites=true&w=majority'

mongo = PyMongo(app)

usuario = mongo.db.usuario
artista_musical = mongo.db.artista_musical
curte_artista_musical = mongo.db.curte_artista_musical
filme = mongo.db.filme
curte_filme = mongo.db.curte_filme
reg = mongo.db.reg
sugestao_filme = mongo.db.sugestao_filme
sugestao_artista = mongo.db.sugestao_artista
sugestao_amigo = mongo.db.sugestao_amigo
registra = mongo.db.registra

def existRegistra(person, colleague):
    return bool(mongo.db.registra.find_one({'usr1': person, 'usr2': colleague}) or mongo.db.registra.find_one({'usr1': colleague, 'usr2': person}))

def existArtista(uri):
    return bool(mongo.db.artista_musical.find_one({'banda_uri': uri}))

def existCurteArtista(person, banda_uri):
    return bool(mongo.db.curte_artista_musical.find_one({'usr': person, 'banda_uri': banda_uri}))

def existFilme(uri):
    return bool(mongo.db.filme.find_one({'filme_uri': uri}))

def existCurteFilme(person, filme_uri):
    return bool(mongo.db.curte_filme.find_one({'usr': person, 'filme_uri': filme_uri}))

@app.after_request
def allow_cross_domain(response: Response):
    """Hook to set up response headers."""
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'content-type'
    return response

@app.route('/')
def index():
    if 'login' in session:
        return jsonify('Você está logado como ' + session['login'])
    return jsonify('Opa, o index é aqui!')

@app.route('/login', methods=['POST'])
def login():
    login = request.json['login']
    senha = request.json['password']
    existe_usuario = reg.find_one({'login': login})

    if login == "DI1902rafaelramos" and senha == "123456":
        session['login'] = login
        return jsonify(session['login'])

    if existe_usuario:
        if bcrypt.hashpw(senha.encode('utf-8'), existe_usuario['senha']) == existe_usuario['senha']:
            session['login'] = login
            return jsonify(session['login'])
    resp = jsonify('Usuário ou senha incorretos!')
    resp.status_code = 401
    return resp




@app.route('/adicionar', methods=['GET', 'POST'])
def adicionar():
    if request.method == 'POST':
        person = request.json['usr1']
        colleague = request.json['usr2']

        if not existRegistra(person, colleague):
            reg1 = {
            'usr1': person,
            'usr2': colleague
            }
            reg2 = {
            'usr1': colleague,
            'usr2': person
            }
            registra.insert_one(reg1)
            registra.insert_one(reg2)

            resp = jsonify('Usuários adicionados com sucesso!')
            resp.status_code = 200
            return resp
        else:
            resp = jsonify('Usuários já são amigos!')
            resp.status_code = 500
            return resp


@app.route('/curtir_filme', methods=['GET', 'POST'])
def curtir_fil():
    if request.method == 'POST':
        usr = request.json['usr']
        filme_uri = request.json['filme_uri']
        filme_nome = request.json['filme_nome']
        nota = request.json['nota']
            
        if not existCurteFilme(usr, filme_uri):   
            if not existFilme(filme_uri):
                fil = {
                'filme_uri': filme_uri,
                'filme_nome': filme_nome
                }
                filme.insert_one(fil) 
            curt = {
            'usr': usr,
            'filme_uri': filme_uri,
            'filme_nome': filme_nome,
            'nota': nota
            }
            
            curte_filme.insert_one(curt)

            resp = jsonify('Usuários curtiu com sucesso!')
            resp.status_code = 200
            return resp

        else:
            resp = jsonify('Usuário já curtiu!')
            resp.status_code = 500
            return resp


@app.route('/curtir_artista', methods=['GET', 'POST'])
def curtir_band():
    if request.method == 'POST':
        usr = request.json['usr']
        banda_uri = request.json['banda_uri']
        banda_nome = request.json['banda_nome']
        nota = request.json['nota']
            
        if not existCurteArtista(usr, banda_uri):   
            if not existArtista(banda_uri):
                band = {
                'banda_uri': banda_uri,
                'banda_nome': banda_nome
                }
                artista_musical.insert_one(band)
            curt = {
            'usr': usr,
            'banda_uri': banda_uri,
            'banda_nome': banda_nome,
            'nota': nota
            }
            
            curte_artista_musical.insert_one(curt)

            resp = jsonify('Usuários curtiu com sucesso!')
            resp.status_code = 200
            return resp

        else:
            resp = jsonify('Usuário já curtiu!')
            resp.status_code = 500
            return resp

@app.route('/registrar', methods=['GET', 'POST'])
def registrar():
    if request.method == 'POST':
        login = request.json['login']
        senha = request.json['password']
        nome = request.json['name']
        cidade_natal = request.json['birthtown']
        nascimento = request.json['birthdate']
        nascimento = nascimento.split('-')
        ano = int(nascimento[0])
        mes = int(nascimento[1])
        dia = int(nascimento[2])

        existe_usuario = usuario.find_one({'login': login})

        if not existe_usuario:

            hashpass = bcrypt.hashpw(senha.encode('utf-8'), bcrypt.gensalt())

            user = {
                'login': login,
                'nome_completo': nome,
                'cidade_natal': cidade_natal,
                'data_nascimento': datetime(ano, mes, dia)
            }
            regs = {
                'login': login,
                'senha': hashpass
            }
            print(user)
            id = usuario.insert_one(user)
            reg.insert_one(regs)
            session['login'] = login
            print(id)
            resp = jsonify('Usuário adicionado com sucesso!')
            resp.status_code = 200
            return resp
        else:
            resp = jsonify('Usuário já existe!')
            resp.status_code = 500
            return resp


@app.route('/usuarios')
def users():
    nomes = []
    users = usuario.find()
    resp = dumps(users)
    return resp

@app.route('/artistas_musicais/<login>')
def artistas(login):
    nomes = []
    artistas = curte_artista_musical.find({'usr': login})
    for artista in artistas:
        nomes.append(artista['banda_nome'])
        print(artista['banda_nome'])
    resp = dumps(nomes)
    return resp

@app.route('/amigos/<login>')
def amigos(login):
    nomes = []
    persons = registra.find({'usr1': login})
    for person in persons:
        nomes.append(usuario.find_one({'login': person['usr2']}))
        print(person['usr2'])
    resp = dumps(nomes)
    return resp

@app.route('/filmes/<login>')
def filmes(login):
    nomes = []
    films = curte_filme.find({'usr': login})
    for film in films:
        nomes.append(film['filme_nome'])
        print(film['filme_nome'])
    resp = dumps(nomes)
    return resp

@app.route('/sugestao_filme/<login>')
def sugest_filme(login):
    nomes = []
    sug = sugestao_filme.find_one({'login': login})
    nomes.append(sug['sugestao1'])
    nomes.append(sug['sugestao2'])
    nomes.append(sug['sugestao3'])
    resp = dumps(nomes)
    return resp

@app.route('/sugestao_artista/<login>')
def sugest_artista(login):
    nomes = []
    sug = sugestao_artista.find_one({'login': login})
    nomes.append(sug['sugestao1'])
    nomes.append(sug['sugestao2'])
    nomes.append(sug['sugestao3'])
    resp = dumps(nomes)
    return resp

@app.route('/sugestao_amigo/<login>')
def sugest_amigo(login):
    amigos = []
    sug = sugestao_amigo.find_one({'login': login})
    amigos.append(usuario.find_one({'login': sug['sugestao1']}))
    amigos.append(usuario.find_one({'login': sug['sugestao2']}))
    amigos.append(usuario.find_one({'login': sug['sugestao3']}))
    resp = dumps(amigos)
    return resp

@app.route('/user/<login>')
def user(login):
    user = usuario.find_one({'login': login})
    resp = dumps(user)
    return resp


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == "__main__":
    app.run()
