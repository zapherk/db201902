#   ----------  BANCO DE DADOS  ----------  #
#   ----------  DATA MINING  ----------  #

from os import system
import psycopg2



conn = psycopg2.connect(dbname="201902The_Life_Snake", user="201902The_Life_Snake",
                        password="425802", host="200.134.10.32")
cur = conn.cursor()
import psycopg2


def medstd(col, table, group):
    cur.execute("select {}, count(*), sum(nota), avg(nota), stddev(nota) from {}  group by {}".format(col, table, group))
    medstd = [now for now in cur.fetchall()]
    for line in medstd:
        print(line)

def maiormed(col, table, group):
    cur.execute("WITH view AS( select {}, count(*) as quantidade, sum(nota), avg(nota), stddev(nota) from {} group by {}) select * from view where quantidade > 1 order by avg DESC".format(col, table, group))
    maiormed = [now for now in cur.fetchall()]
    for line in maiormed:
        print(line)

def maiorpop(col, table, group):
    cur.execute("select {}, count(*) as quantidade, sum(nota) as soma, avg(nota) as media, stddev(nota) as desvpad from {} group by {} order by quantidade desc LIMIT 10".format(col, table, group))
    #adicionado o trecho "order by avg DESC"
    maiorpop = [now for now in cur.fetchall()]
    for line in maiorpop:
        print(line)

def criaConheceNormalizada():
    cur.execute("create view ConheceNormalizada as with aux as (select usr1, usr2 from registra union all select usr2, usr1 from registra) select distinct * from aux;")

def filmesCurtidosDupla():
    cur.execute("select cf1.usr, cf2.usr, count(cf2.filme_uri) from curte_filme cf1, curte_filme cf2, conhecenormalizada cn where cf1.usr = cn.usr1 and cf2.usr = cn.usr2 and cf1.filme_uri = cf2.filme_uri group by cf1.usr, cf2.usr order by count(cf2.filme_uri) desc limit 1;")
    dupla = [now for now in cur.fetchall()]
    for line in dupla:
        print(line)

def amigosGrupo():
    cur.execute("select count(*) from conhecenormalizada where usr1 in (select distinct usr2 from conhecenormalizada where usr1 in ('http://utfpr.edu.br/CSB30/2019/2/DI1902rafaelramos', 'http://utfpr.edu.br/CSB30/2019/2/DI1902raphaelfelix', 'http://utfpr.edu.br/CSB30/2019/2/DI1902moisesdias'));")
    num = [now for now in cur.fetchall()]
    for line in num:
        print(line)

#Qual é a média e desvio padrão dos ratings para artistas musicais e filmes?
#medstd("filme_uri", "curte_filme", "filme_uri")
#medstd("banda_uri", "curte_artista_musical", "banda_uri")

#Quais são os artistas e filmes com o maior rating médio curtidos por pelo menos duas pessoas? Ordenados por rating médio.
#maiormed("filme_uri", "curte_filme", "filme_uri")
#maiormed("banda_uri", "curte_artista_musical", "banda_uri")

#Quais são os 10 artistas musicais e filmes mais populares? Ordenados por popularidade.
#maiorpop("filme_uri", "curte_filme", "filme_uri")
#maiorpop("banda_uri", "curte_artista_musical", "banda_uri")

#Crie uma view chamada ConheceNormalizada que represente simetricamente os relacionamentos de conhecidos da turma. Por exemplo, se a conhece b mas b não declarou conhecer a, a view criada deve conter o relacionamento (b,a) além de (a,b).
#cur.execute("drop view conhecenormalizada;")
#criaConheceNormalizada()
#conn.commit()
#conn.close()

#Quais são os conhecidos (duas pessoas ligadas na view ConheceNormalizada) que compartilham o maior numero de filmes curtidos?
#filmesCurtidosDupla()

#Qual o número de conhecidos dos conhecidos (usando ConheceNormalizada) para cada integrante do seu grupo?
#amigosGrupo()