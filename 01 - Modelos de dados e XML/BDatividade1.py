#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import csv
from xml.dom.minidom import parse
import xml.dom.minidom

if not os.path.exists('dadosMarvel'):
    os.makedirs('dadosMarvel')
if not os.path.exists('dadosMarvel/herois.csv'):
    open('dadosMarvel/herois.csv', 'w')
if not os.path.exists('dadosMarvel/herois_good.csv'):
    open('dadosMarvel/herois_good.csv', 'w')
if not os.path.exists('dadosMarvel/herois_bad.csv'):
    open('dadosMarvel/herois_bad.csv', 'w')

herois_file = open('dadosMarvel/herois.csv', 'w')
herois_good_file = open('dadosMarvel/herois_good.csv', 'w')
herois_bad_file = open('dadosMarvel/herois_bad.csv', 'w')

herois = csv.writer(herois_file)
herois_good = csv.writer(herois_good_file)
herois_bad = csv.writer(herois_bad_file)

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement

# Get all the heroes in the universe
lista_herois = universe.getElementsByTagName("hero")

head = ['name', 'popularity', 'alignment', 'gender', 'height_m', 'weight_kg', 'hometown', 'intelligence', 'strength',
                 'speed', 'durability', 'energy_Projection', 'fighting_Skills']

herois.writerow(head)
herois_good.writerow(head)
herois_bad.writerow(head)

numMaus = 0
numBons = 0
numTotal = 0
somaPeso = 0
imcHulk = 0.0

for hero in lista_herois:
    heroes_info = []
    for category in head:
        heroes_info.append(hero.getElementsByTagName(category)[0].childNodes[0].data)
    herois.writerow(heroes_info)
    if (hero.getElementsByTagName('alignment')[0].childNodes[0].data == 'Good'):
        herois_good.writerow(heroes_info)
        numBons += 1
    if (hero.getElementsByTagName('alignment')[0].childNodes[0].data == 'Bad'):
        herois_bad.writerow(heroes_info)
        numMaus += 1
    numTotal += 1
    somaPeso += int(hero.getElementsByTagName('weight_kg')[0].childNodes[0].data)
    if(hero.getElementsByTagName('name')[0].childNodes[0].data == 'Hulk'):
        imcHulk = int(hero.getElementsByTagName('weight_kg')[0].childNodes[0].data)/(int(
            hero.getElementsByTagName('height_m')[0].childNodes[0].data)**2)


print('%.2f é a proporção entre heróis bons e maus' %(numBons/numMaus))
print('%.2f é a média dos pesos' %(somaPeso/numTotal))
print('%.2f é o imc do hulk' % imcHulk)

