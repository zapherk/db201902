CREATE TABLE usuario(
	login VARCHAR(12) NOT NULL,
	nome_completo VARCHAR(30) NOT NULL,
	cidade_natal VARCHAR(15) NOT NULL,
	PRIMARY KEY(login)
);

CREATE TABLE registra(
	usr1 VARCHAR(12) NOT NULL,
	usr2 VARCHAR(12) NOT NULL,
	PRIMARY KEY(usr1, usr2),
	FOREIGN KEY(usr1) REFERENCES usuario(login)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	FOREIGN KEY(usr2) REFERENCES usuario(login)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE bloqueia(
	usr1 VARCHAR(12) NOT NULL,
	usr2 VARCHAR(12) NOT NULL,
	razao_bloqueio VARCHAR(200),
	PRIMARY KEY(usr1, usr2),
	FOREIGN KEY(usr1) REFERENCES usuario(login)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	FOREIGN KEY(usr2) REFERENCES usuario(login)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE musico(
	nome_real VARCHAR(30) NOT NULL,
	estilo_musical VARCHAR(15),
	data_nascimento DATE,
	PRIMARY KEY(nome_real)
);

CREATE TABLE artista_musical(
	ID INTEGER NOT NULL,
	nome_artistico VARCHAR(30),
	pais VARCHAR(15),
	genero_musical VARCHAR(15),
	PRIMARY KEY(ID)
);

CREATE TABLE possui(
	ID INTEGER NOT NULL,
	nome VARCHAR(30) NOT NULL,
	PRIMARY KEY(ID, nome),
	FOREIGN KEY(ID) REFERENCES artista_musical(ID)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
	FOREIGN KEY(nome) REFERENCES musico(nome_real)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
);

CREATE TABLE categoria(
    nome VARCHAR(20) NOT NULL,
    superior VARCHAR(20),
    PRIMARY KEY(nome),
    FOREIGN KEY(superior) REFERENCES categoria(nome)
	ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE filme(
    ISBN INTEGER NOT NULL,
    nome VARCHAR(40) NOT NULL,
    data_lancamento DATE,
    nome_categoria VARCHAR(20),
    PRIMARY KEY(ISBN),
    FOREIGN KEY(nome_categoria) REFERENCES categoria(nome)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE curte_filme(
    usr VARCHAR(12) NOT NULL,
    ISBN INTEGER NOT NULL,
    nota DECIMAL,
    PRIMARY KEY(usr, ISBN),
    FOREIGN KEY(usr) REFERENCES usuario(login)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(ISBN) REFERENCES filme(ISBN)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE equipe_cinema(
    ID INTEGER NOT NULL,
    telefone VARCHAR(20),
    endereco VARCHAR(30),
    PRIMARY KEY(ID)
);

CREATE TABLE dirige(
    ISBN INTEGER NOT NULL,
    id_diretor INTEGER NOT NULL,
    salario DECIMAL,
    PRIMARY KEY(ISBN),
    FOREIGN KEY(ISBN) REFERENCES filme(ISBN)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(id_diretor) REFERENCES equipe_cinema(ID)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE atua(
    ISBN INTEGER NOT NULL,
    id_ator INTEGER NOT NULL,
    salario DECIMAL NOT NULL,
    PRIMARY KEY(ISBN, id_ator),
    FOREIGN KEY(ISBN) REFERENCES filme(ISBN)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(id_ator) REFERENCES equipe_cinema(ID)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE curte_artista_musical(
    usr VARCHAR(12) NOT NULL,
    ID INTEGER NOT NULL,
    nota DECIMAL,
    PRIMARY KEY(usr, ID),
    FOREIGN KEY(usr) REFERENCES usuario(login)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(ID) REFERENCES artista_musical(ID)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

INSERT INTO usuario VALUES ('Rafa', 'LABIC', 'Ilheus');
INSERT INTO usuario VALUES ('Felix', 'UTFPR', 'Minas');
INSERT INTO usuario VALUES ('Moises', 'UTFPR', 'Curitiba');

INSERT INTO registra VALUES ('Rafa', 'Moises');
INSERT INTO registra VALUES ('Rafa', 'Felix');
INSERT INTO registra VALUES ('Felix', 'Moises');

INSERT INTO bloqueia VALUES ('Rafa', 'Moises', 'Chato demais.');
INSERT INTO bloqueia VALUES ('Rafa', 'Felix', 'Otaku.');
INSERT INTO bloqueia VALUES ('Felix', 'Moises');

INSERT INTO musico VALUES ('Joel Zimmermann', 'Electro', '5/03/1981');
INSERT INTO musico VALUES ('Thiago Potter', 'Pagode', '5/12/1971');
INSERT INTO musico VALUES ('Draco Badfoi', 'Forro', '9/12/1841');

INSERT INTO artista_musical VALUES ('1', 'Deadmau5', 'Canada', 'Eletronica');
INSERT INTO artista_musical VALUES ('2', 'Tchan', 'Brasil', 'Pagode');
INSERT INTO artista_musical VALUES ('3', 'Draquera', 'Brasil', 'Forro');

INSERT INTO curte_artista_musical VALUES ('Rafa', '1', 10.0);
INSERT INTO curte_artista_musical VALUES ('Moises', '2');
INSERT INTO curte_artista_musical VALUES ('Felix', '3', 8.4);

INSERT INTO categoria VALUES ('Alegria');
INSERT INTO categoria VALUES ('Terror');
INSERT INTO categoria VALUES ('Comedia');

INSERT INTO filme VALUES ('1414', 'As cronicas de banco de dados', '14/08/2019', 'Alegria');
INSERT INTO filme VALUES ('1515', 'Se beber nao programe', '12/02/2017', 'Comedia');
INSERT INTO filme VALUES ('1616', 'O curioso caso da integral tripla', '10/12/2018', 'Terror');

INSERT INTO curte_filme VALUES ('Rafa', '1414', '10.0');
INSERT INTO curte_filme VALUES ('Felix', '1515', '8.2');
INSERT INTO curte_filme VALUES ('Rafa', '1616', '9.8');

INSERT INTO equipe_cinema VALUES ('1', '9999-9999', 'Rua da Amargura');
INSERT INTO equipe_cinema VALUES ('2', '8888-8888', 'Rua da Alegria');
INSERT INTO equipe_cinema VALUES ('3', '7777-7777', 'Rua da Loucura');
INSERT INTO equipe_cinema VALUES ('10', '8888-7777', 'Rua do Diretor');
INSERT INTO equipe_cinema VALUES ('11', '8888-9967', 'Rua do Diretor 2');

INSERT INTO atua VALUES ('1414', '1', '1000');
INSERT INTO atua VALUES ('1515', '1', '1500');
INSERT INTO atua VALUES ('1616', '3', '2000');

INSERT INTO dirige VALUES ('1414', '10', '10000');
INSERT INTO dirige VALUES ('1515', '11', '5000');
INSERT INTO dirige VALUES ('1616', '11', '4500');

INSERT INTO possui VALUES ('1', 'Joel Zimmermann');
INSERT INTO possui VALUES ('2', 'Thiago Potter');
INSERT INTO possui VALUES ('3', 'Draco Badfoi');
