CREATE TABLE usuario(
	login VARCHAR(150) NOT NULL,
	nome_completo VARCHAR(45) NOT NULL,
	cidade_natal VARCHAR(20),
    data_nascimento DATE,
	PRIMARY KEY(login)
);

CREATE TABLE registra(
	usr1 VARCHAR(150) NOT NULL,
	usr2 VARCHAR(150) NOT NULL,
	PRIMARY KEY(usr1, usr2),
	FOREIGN KEY(usr1) REFERENCES usuario(login)
		ON DELETE CASCADE
		ON UPDATE NO ACTION,
	FOREIGN KEY(usr2) REFERENCES usuario(login)
		ON DELETE CASCADE
		ON UPDATE NO ACTION
);

CREATE TABLE artista_musical(
	banda_uri VARCHAR(150) NOT NULL,
	PRIMARY KEY(banda_uri)
);


CREATE TABLE filme(
    filme_uri VARCHAR(150) NOT NULL,
    PRIMARY KEY(filme_uri)
);

CREATE TABLE curte_filme(
    usr VARCHAR(150) NOT NULL,
    filme_uri VARCHAR(150) NOT NULL,
    nota DECIMAL,
    PRIMARY KEY(usr, filme_uri),
    FOREIGN KEY(usr) REFERENCES usuario(login)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(filme_uri) REFERENCES filme(filme_uri)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE curte_artista_musical(
    usr VARCHAR(150) NOT NULL,
    banda_uri VARCHAR(150) NOT NULL,
    nota DECIMAL,
    PRIMARY KEY(usr, banda_uri),
    FOREIGN KEY(usr) REFERENCES usuario(login)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(banda_uri) REFERENCES artista_musical(banda_uri)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

/* FAST DROP

DROP TABLE usuario CASCADE;
DROP TABLE artista_musical CASCADE;
DROP TABLE curte_filme CASCADE;
DROP TABLE curte_artista_musical CASCADE;
DROP TABLE filme CASCADE;
DROP TABLE registra CASCADE;

*/