#   ----------  BANCO DE DADOS  ----------  #
#   ----------  INTERFACE CRUD  ----------  #

from os import system

import psycopg2



conn = psycopg2.connect(dbname="201902The_Life_Snake", user="201902The_Life_Snake",
                        password="425802", host="200.134.10.32")
cur = conn.cursor()


def exist(table, col1, one, col2=None, two=None):
    if two is None:
        cur.execute(
            "SELECT * FROM {} WHERE {} = '{}'".format(table, col1, one))
    else:
        cur.execute(
            "SELECT * FROM {} WHERE {} = '{}' AND {} = '{}'".format(table, col1, one, col2, two))

    if len(cur.fetchall()) > 0:
        return True
    else:
        return False


class Menu:

    @staticmethod
    def showOptions():

        print()
        print()
        print()

        print("#   ----------  BANCO DE DADOS  ----------  #")
        print("#   ----------  INTERFACE CRUD  ----------  #")
        print()
        print("ATENÇÃO: Os items (*) são obrigatórios para ingressar à rede social.")
        print()
        print("O que deseja fazer?")
        print()
        print("1) Cadastrar pessoa")
        print("2) Listar pessoas")
        print("0) Sair")
        print()

        op = input("Opção: ")

        return op
                
    @staticmethod
    def choice(op):

        if op == '1':

            system('cls')
            print()

            print("#   ----------  BANCO DE DADOS  ----------  #")
            print("#   ----------  INTERFACE CRUD  ----------  #")
            print()
            print("#   ----------  CADASTRO DE USUÁRIO  ----------  #")
            print()

            login = input("(*) Login: ")
            nome_completo = input("(*) Nome completo: ")
            cidade_natal = input("Cidade natal: ")
            data_nascimento = input("Data de nascimento (dd/mm/aaaa): ")

            if data_nascimento == '':
                data_nascimento = None

            if login == '' or nome_completo == '':
                print()
                print("Preencha os campos obrigatórios!")

            else:
                Usuario.cadastrar(login, nome_completo,
                                  cidade_natal, data_nascimento)

        if op == '2':
            
            print()

            print("#   ----------  BANCO DE DADOS  ----------  #")
            print("#   ----------  INTERFACE CRUD  ----------  #")

            cur.execute("SELECT login FROM usuario")
            logins = [row[0] for row in cur.fetchall()]
            cur.execute("SELECT nome_completo FROM usuario")
            names = [row[0] for row in cur.fetchall()]

            print("Login --------------------------------------------------------------- Name")

            for login, name in zip(logins, names):
                print()
                print()
                print(login, " ---- ", name)

            cho = Menu.listOptions()
            Menu.listChoice(cho)


    @staticmethod
    def listOptions():
        
        print()
        print()
        print("O que deseja fazer?")
        print()
        print("1) Cadastrar um conhecido")
        print("2) Apagar uma pessoa")
        print("3) Editar uma pessoa")
        print("0) Sair")
        print()

        op = input("Opção: ")

        return op

    @staticmethod
    def listChoice(op):

        print()

        print("#   ----------  BANCO DE DADOS  ----------  #")
        print("#   ----------  INTERFACE CRUD  ----------  #")

        if op == '1':
            system('cls')
            print()

            print("#   ----------  BANCO DE DADOS  ----------  #")
            print("#   ----------  INTERFACE CRUD  ----------  #")
            print()
            print("#   ----------  CADASTRO DE CONHECIDOS  ----------  #")
            print()
            print()
            print()
            print("ATENÇÃO: Os items (*) são obrigatórios para ingressar à rede social.")
            print()

            login1 = input("(*) Seu login: ")
            login2 = input("(*) Login do seu conhecido: ")


            if login1 == '' or login2 == '':
                print()
                print("Preencha os campos obrigatórios!")

            else:
                Usuario.registra(login1, login2)

        if op == '2':
            system('cls')
            print()

            print("#   ----------  BANCO DE DADOS  ----------  #")
            print("#   ----------  INTERFACE CRUD  ----------  #")
            print()
            print("#   ----------  DELETAR USUÁRIO  ----------  #")
            print()
            print()
            login = input("(*) Login a ser deletado: ")


            if login == '':
                print("Preencha o login a ser deletado!")

            else:
                Usuario.deletar(login)

        if op == '3':
            system('cls')
            print()

            print("#   ----------  BANCO DE DADOS  ----------  #")
            print("#   ----------  INTERFACE CRUD  ----------  #")
            print()
            print("#   ----------  DELETAR USUÁRIO  ----------  #")
            print()
            print()
            print()
            print("Digite os campos que deseja editar, deixe em branco o que não quer atualizar!")
            print()
            login = input("Login a ser editado: ")


            if login == '':
                print("Digite o login a ser editado!")

            nome_completo = input("Nome Completo: ")
            cidade_natal = input("Cidade natal: ")
            data_nascimento = input("Data de nascimento: ")

            if nome_completo != '':
                Usuario.editar_nome(login, nome_completo)
                print()
                print("Nome atualizado com sucesso!")
            
            if cidade_natal != '':
                Usuario.editar_cidade_natal(login, cidade_natal)
                print()
                print("Cidade natal atualizada com sucesso!")

            if data_nascimento != '':
                Usuario.editar_data_nascimento(login, data_nascimento)
                print()
                print("Data de nascimento atualizada com sucesso!")
            

#   ----------  CRUD FUNCTIONS   ---------- #


class Usuario:

    @staticmethod
    def cadastrar(login, nome_completo, cidade_natal=None, data_nascimento=None):
        if not exist("usuario", "login", login):
            cur.execute("INSERT INTO usuario VALUES (%s, %s, %s, %s)", (
                login, nome_completo, cidade_natal, data_nascimento))
            print()
            print("Usuário cadastrado com sucesso.")
        else:
            print()
            print("Usuário já existe.")
        conn.commit()

    @staticmethod
    def deletar(login):
        if exist("usuario", "login", login):
            cur.execute(
                "DELETE from usuario WHERE login = '{}'".format(login))
            print()
            print("Usuário deletado com sucesso.")
        else:
            print()
            print("Usuário não encontrado.")
        conn.commit()

    @staticmethod
    def editar_nome(login, nome_completo):
        if exist("usuario", "login", login):
            cur.execute(
                "UPDATE usuario SET nome_completo = '{}' WHERE login = '{}'".format(
                    nome_completo, login))
        else:
            print()
            print("Usuário não encontrado.")
        conn.commit()

    @staticmethod
    def editar_cidade_natal(login, cidade_natal):
        if exist("usuario", "login", login):
            cur.execute(
                "UPDATE usuario SET cidade_natal = '{}' WHERE login = '{}'".format(
                    cidade_natal, login))
        else:
            print()
        conn.commit()

    @staticmethod
    def editar_data_nascimento(login, data_nascimento):
        if exist("usuario", "login", login):
            cur.execute(
                "UPDATE usuario SET data_nascimento = %s WHERE login = %s", (data_nascimento, login))
        else:
            print("Usuário não encontrado.")
        conn.commit()

    @staticmethod
    def registra(login1, login2):
        if not exist("registra", "usr1", login1, "usr2", login2) and (login1 != login2):
            cur.execute(
                "INSERT INTO registra VALUES ('{}', '{}')".format(login1,
                                                                  login2))
            print()
            print("Usuários linkados com sucesso.")
        elif (login1 == login2):
            print()
            print("Usuário não pode registrar a si mesmo.")
        else:
            print()
            print("Usuarios já linkados!")
        conn.commit()


while True:
    choice = Menu.showOptions()
    Menu.choice(choice)
    if(choice == '0'):
        break
